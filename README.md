# Client Message Server demo with Sockets (Authentication and authorization)
This is a demo miniproject of sockets implementation in python. 
The application implements a Message server returning and waiting for JSON objects, and a client that sends commands to the server.
The server implements a simple authorization and authentication logic,  as a simple message send and read logic.

## Download
To download the project please click on the download button on the repository [main page](https://gitlab.com/client-server-sqlite)
or run the command below: 
`git clone https://gitlab.com/client-server-sqlite`

## Install and run
### Requirements
For the application to run on your machine you will need:
-  [Python3](https://www.python.org/downloads/) installed and added to your [PATH variable](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/).

### Install
To create the  sqlite database, run the set-up.bat batch file

### Run
To start the application you need to use the batch file below:
- tests.bat to start the tests.
- client_server.bat to start the application.

## Server commands
The server will run on localhost with port 8081 and have different options matching user authorization, by default 2 users will be created by the [set-up](https://gitlab.com/zaraczke/client-server/-/blob/master/set-up.bat) batch file as test users: 

- User -> password user (this is a normal user)
- Admin ->  password admin (this is a user with admin authorization)

the general application commands are: 
- help: returns a list of available commands with a brief description,
- login: logs in the user (Input data: username, password),
- register: register a new user (Input data: username, password, repeat password),
- logout: logs out the current user,
- quit: end the connection and close the application

the logged user commands are: 
- message: sends a message to a user (Input data: username, message text),
- readMessages: returns list of messages from a user (Input data: username),
- readHistory: returns a list of all sent and received messages,
- cleanInbox: cleans the inbox from messages (Deletes all the messages),
- changePassword: changes password for the current user (Input data: password),

the admin specifc commands are:
- info: returns server version and created date,
- uptime: returns server life time,
- addUser: add a new user (input data:  username, password, authorization),
- addAuth: add a new user (Input data:  authorization),
- changeUser: change a user data (input data: user_id, optional: username, password, authorization),
- deleteUser: delete a user from database (Input data: user_id),
- deleteMessage: delete a message from database for message moderation (input data: message_id),
- getUsers: gets list of all users,
- getMessages: gets list of all messages for moderation,
- getAuths: gets list of all authorizations,
- communicate: sends communication for all user (Input data: message text).
          

