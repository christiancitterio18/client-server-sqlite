@echo off

echo Setting things up...

cd DB
move "pythondata.db" ".."
cd ..

echo Starting client and server...
start cmd /C python server.py

echo Please enter a command...
python client.py
